# -*- mode : conf -*-
Bootstrap: docker
From: ubuntu:bionic

%environment

    export ROSWELL_HOME=/planner/.roswell
    export PATH=/planner/.roswell/bin:$PATH
    export MAKEFLAGS="-j $((2*$(nproc)))"
    export ASDF_OUTPUT_TRANSLATIONS='(:output-translations ("/planner/.roswell/local-projects/strips/search-instance-dependent" "/tmp/search-instance-dependent/") ("/planner/.roswell/" "/planner/.cache/") :inherit-configuration)'
    export TZ='Asia/Tokyo'

%post
    ## Install all necessary dependencies.
    apt-get update
    apt-get -y install git curl zip ca-certificates build-essential automake libcurl4-openssl-dev
    rm -rf /var/lib/apt/lists/*

    git clone -b ipc-2018-seq-agl https://bitbucket.org/ipc2018-classical/team33.git /planner

    cd /planner

    if ! which ros ; then
        (
        git clone -b release https://github.com/roswell/roswell.git roswell-src
        cd roswell-src
        sh bootstrap
        ./configure
        make
        make install
        ros setup
        )
    fi

    # unpack the irregular dependencies to the directory recognized by quicklisp
    mkdir .roswell
    unzip -o zip/type-r-master.zip     -d .roswell/local-projects/
    unzip -o zip/cl-prolog2-master.zip -d .roswell/local-projects/
    unzip -o zip/cl-cudd-master.zip    -d .roswell/local-projects/

    (
       mkdir -p .roswell/local-projects/strips/
       cp -t .roswell/local-projects/strips/ *.asd
       cp -r -t .roswell/local-projects/strips/ preprocess/ lib/ search/ search-instance-dependent/ validate/
    )

    # this now downloads from quicklisp, not from github.
    ros install trivial-package-manager
    # Quicklisp downloads the rest of dependencies,
    # recognize the roswell script (roswell/alien.ros) and copy it as an executable .roswell/bin/alien .
    ros install strips

    # alternative way to build alien
    ros dynamic-space-size=8000 dump --disable-compression executable roswell/alien.ros -o alien
    ./alien

%runscript
    ## The runscript is called whenever the container is used to solve
    ## an instance.

    pwd
    env
    ls
    echo args: $@
    # use 3600 limit on both agile and SAT; will be terminated anyways
    bash -c "/planner/alien -t 3600 -m 7000 --search-option '(eager (bucket-open-list (sum (product (constant (expt 2 (ceiling (log strips::*op-size* 2)))) (novelty2)) (ff/rpg))))' $*"



## Update the following fields with meta data about your submission.
## Please use the same field names and use only one line for each value.
%labels
Name        alien
Description forward search with a satisficing heuristic not based on lower bound
Authors     Masataro Asai <guicho2.71828@gmail.com>
SupportsDerivedPredicates yes
SupportsQuantifiedPreconditions yes
SupportsQuantifiedEffects yes
